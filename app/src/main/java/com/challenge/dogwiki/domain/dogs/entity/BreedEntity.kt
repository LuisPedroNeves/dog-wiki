package com.challenge.dogwiki.domain.dogs.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BreedEntity(
    val id: Int,
    val name: String,
    val image: String,
    val group: String,
    val origin: String,
    val temperament: String
) : Parcelable