package com.challenge.dogwiki.domain.dogs.usecases

import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.DogsRepository
import com.challenge.dogwiki.domain.dogs.entity.SearchBreedEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SearchDogBreedsUseCase @Inject constructor(private val dogsRepository: DogsRepository) {
    suspend fun invoke(query: String): Flow<BaseResult<SearchBreedEntity, String>> {
        return dogsRepository.searchDogBreeds(query)
    }
}