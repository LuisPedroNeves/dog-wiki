package com.challenge.dogwiki.domain.dogs.usecases

import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.DogsRepository
import com.challenge.dogwiki.domain.dogs.entity.BreedListEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetDogBreedsUseCase @Inject constructor(private val dogsRepository: DogsRepository) {
    suspend fun invoke(pageIndex: Int = 0): Flow<BaseResult<BreedListEntity, String>> {
        return dogsRepository.getDogBreeds(pageIndex)
    }
}