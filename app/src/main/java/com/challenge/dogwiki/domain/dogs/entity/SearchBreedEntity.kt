package com.challenge.dogwiki.domain.dogs.entity

data class SearchBreedEntity(
    val list: List<BreedEntity> = emptyList(),
    val query: String = ""
)