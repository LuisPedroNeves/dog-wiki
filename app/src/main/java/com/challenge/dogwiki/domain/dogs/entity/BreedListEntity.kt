package com.challenge.dogwiki.domain.dogs.entity

data class BreedListEntity(
    val list: List<BreedEntity> = emptyList(),
    val pageIndex: Int = 0,
    var isListSorted: Boolean = true,
    var isGridView: Boolean = true
)