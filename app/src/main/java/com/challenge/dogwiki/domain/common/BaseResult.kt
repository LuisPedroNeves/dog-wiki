package com.challenge.dogwiki.domain.common

sealed class BaseResult <out T : Any, out U : Any> {
    data class Success <T: Any>(val data : T) : BaseResult<T, Nothing>()
    data class Error <T : Any, U : Any>(val error: U, val data: T? = null) : BaseResult<T, U>()
}