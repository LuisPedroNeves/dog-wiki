package com.challenge.dogwiki.domain.dogs

import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.entity.BreedListEntity
import com.challenge.dogwiki.domain.dogs.entity.SearchBreedEntity
import kotlinx.coroutines.flow.Flow

interface DogsRepository {
    suspend fun getDogBreeds(pageIndex: Int = 0): Flow<BaseResult<BreedListEntity, String>>
    suspend fun searchDogBreeds(query: String): Flow<BaseResult<SearchBreedEntity, String>>
}