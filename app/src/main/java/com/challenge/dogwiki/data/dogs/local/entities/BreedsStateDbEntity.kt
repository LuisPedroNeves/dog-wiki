package com.challenge.dogwiki.data.dogs.local.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "breeds_state", indices = [Index(value = ["id"], unique = true)])
data class BreedsStateDbEntity(
    @PrimaryKey
    var id : Int,
    @ColumnInfo(name = "lastQueriedPage")
    var lastQueriedPage: Int
)