package com.challenge.dogwiki.data.dogs.remote.repository

import com.challenge.dogwiki.data.common.exceptions.NoInternetConnection
import com.challenge.dogwiki.data.dogs.local.BreedListDao
import com.challenge.dogwiki.data.dogs.local.entities.BreedDbEntity
import com.challenge.dogwiki.data.dogs.local.entities.BreedsStateDbEntity
import com.challenge.dogwiki.data.dogs.remote.api.DogsApi
import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.DogsRepository
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import com.challenge.dogwiki.domain.dogs.entity.BreedListEntity
import com.challenge.dogwiki.domain.dogs.entity.SearchBreedEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DogsRepositoryImpl @Inject constructor(
    private val dogsApi: DogsApi,
    private val breedListDao: BreedListDao
) :
    DogsRepository {

    companion object {
        private const val NON_AVAILABLE = "N/A"
        private const val IMAGE_BASE_URL = "https://cdn2.thedogapi.com/images/%s_1280.jpg"
        private const val BREED_LIST_DB_INDEX = 0
    }

    override suspend fun getDogBreeds(pageIndex: Int): Flow<BaseResult<BreedListEntity, String>> {
        return flow {
            try {
                val response = dogsApi.getDogBreeds(page = pageIndex)

                if (!response.isSuccessful) {
                    emit(BaseResult.Error(response.message()))
                    return@flow
                }

                val body = response.body() ?: throw java.lang.Exception()

                val breedsDbList = body.map { breed ->
                    BreedDbEntity(
                        id = breed.id,
                        breedListId = BREED_LIST_DB_INDEX,
                        name = breed.name,
                        image = breed.imageResponse.url.takeIf { !it.isNullOrEmpty() } ?: NON_AVAILABLE,
                        group = breed.breedGroup.takeIf { !it.isNullOrEmpty() } ?: NON_AVAILABLE,
                        origin = breed.origin.takeIf { !it.isNullOrEmpty() } ?: NON_AVAILABLE,
                        temperament = breed.temperament.takeIf { !it.isNullOrEmpty() } ?: NON_AVAILABLE,
                    )
                }

                if (breedListDao.getBreedState() == null) {
                    val breedListDb = BreedsStateDbEntity(0, pageIndex)
                    breedListDao.insert(breedListDb)
                } else {
                    breedListDao.updateLastQueriedPage(pageIndex)
                }

                breedListDao.insertBreeds(breedsDbList)

                val result = readBreedsFromDb()
                emit(BaseResult.Success(result))

            } catch (e: NoInternetConnection) {
                val result = readBreedsFromDb()
                emit(BaseResult.Error(e.message ?: "No internet connection.", result))

            } catch (e: Exception) {
                emit(BaseResult.Error(e.message ?: "An error occurred. Try again later."))
            }
        }
    }

    private fun readBreedsFromDb(): BreedListEntity {
        val queryResult = breedListDao.getListWithBreeds()
        val list = queryResult.first().breeds.map {
            BreedEntity(
                id = it.id,
                name = it.name,
                image = it.image,
                group = it.group,
                origin = it.origin,
                temperament = it.temperament,
            )
        }

        return BreedListEntity(list, queryResult.first().breedState.lastQueriedPage)
    }

    override suspend fun searchDogBreeds(query: String): Flow<BaseResult<SearchBreedEntity, String>> {
        return flow {
            try {
                val response = dogsApi.searchDogBreed(query = query)

                if (!response.isSuccessful) {
                    emit(BaseResult.Error(response.message()))
                    return@flow
                }
                val body = response.body() ?: throw java.lang.Exception()

                val breedsList = body.map { breed ->
                    BreedEntity(
                        id = breed.id,
                        name = breed.name,
                        image = String.format(IMAGE_BASE_URL, breed.referenceImageId),
                        group = breed.breedGroup.takeIf { !it.isNullOrEmpty() } ?: NON_AVAILABLE,
                        origin = breed.origin.takeIf { !it.isNullOrEmpty() } ?: NON_AVAILABLE,
                        temperament = breed.temperament.takeIf { !it.isNullOrEmpty() } ?: NON_AVAILABLE,
                    )
                }

                emit(BaseResult.Success(SearchBreedEntity(breedsList, query)))

            } catch (e: NoInternetConnection) {
                emit(BaseResult.Error(e.message ?: "No internet connection."))

            } catch (e: Exception) {
                emit(BaseResult.Error(e.message ?: "An error occurred. Try again later."))
            }
        }
    }
}