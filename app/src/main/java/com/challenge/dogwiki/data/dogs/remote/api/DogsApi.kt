package com.challenge.dogwiki.data.dogs.remote.api

import com.challenge.dogwiki.data.dogs.remote.dto.*
import retrofit2.Response
import retrofit2.http.*

interface DogsApi {
    @GET("v1/breeds")
    suspend fun getDogBreeds(
        @Query("limit") pageSize: Int = 20,
        @Query("page") page: Int = 0
    ): Response<BreedsResponse>

    @GET("v1/breeds/search")
    suspend fun searchDogBreed(
        @Query("q") query: String
    ): Response<BreedsResponse>
}