package com.challenge.dogwiki.data.dogs.remote.dto

import com.google.gson.annotations.SerializedName

data class ImageResponse(
    @SerializedName("height")
    val height: Int,
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String?,
    @SerializedName("width")
    val width: Int
)