package com.challenge.dogwiki.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.challenge.dogwiki.data.dogs.local.BreedListDao
import com.challenge.dogwiki.data.dogs.local.entities.BreedDbEntity
import com.challenge.dogwiki.data.dogs.local.entities.BreedsStateDbEntity

@Database(
    entities = [
        BreedsStateDbEntity::class,
        BreedDbEntity::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase(){
    abstract fun breedListDao() : BreedListDao
}