package com.challenge.dogwiki.data.dogs

import com.challenge.dogwiki.data.AppDatabase
import com.challenge.dogwiki.data.common.NetworkModule
import com.challenge.dogwiki.data.dogs.local.BreedListDao
import com.challenge.dogwiki.data.dogs.remote.api.DogsApi
import com.challenge.dogwiki.data.dogs.remote.repository.DogsRepositoryImpl
import com.challenge.dogwiki.domain.dogs.DogsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [NetworkModule::class])
@InstallIn(SingletonComponent::class)
class DogsModule {

    @Singleton
    @Provides
    fun provideDogsApi(retrofit: Retrofit): DogsApi {
        return retrofit.create(DogsApi::class.java)
    }

    @Provides
    @Singleton
    fun provideBreedListDao(appDatabase: AppDatabase) : BreedListDao {
        return appDatabase.breedListDao()
    }

    @Singleton
    @Provides
    fun provideDogsRepository(dogsApi: DogsApi, breedListDao: BreedListDao): DogsRepository {
        return DogsRepositoryImpl(dogsApi, breedListDao)
    }
}