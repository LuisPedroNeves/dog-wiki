package com.challenge.dogwiki.data.dogs.local.entities

import androidx.room.*

@Entity(
    tableName = "breed",
    indices = [Index(value = ["id"], unique = true)],
    foreignKeys = [ForeignKey(entity = BreedsStateDbEntity::class, parentColumns = ["id"], childColumns = ["breedListId"])]
)
data class BreedDbEntity(
    @PrimaryKey
    val id: Int,
    @ColumnInfo(name = "breedListId")
    val breedListId: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "image")
    val image: String,
    @ColumnInfo(name = "group")
    val group: String,
    @ColumnInfo(name = "origin")
    val origin: String,
    @ColumnInfo(name = "temperament")
    val temperament: String
)