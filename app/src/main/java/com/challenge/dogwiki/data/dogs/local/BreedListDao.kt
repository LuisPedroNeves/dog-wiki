package com.challenge.dogwiki.data.dogs.local

import androidx.room.*
import com.challenge.dogwiki.data.dogs.local.entities.BreedDbEntity
import com.challenge.dogwiki.data.dogs.local.entities.BreedsStateDbEntity
import com.challenge.dogwiki.data.dogs.local.entities.ListWithBreeds

@Dao
interface BreedListDao {

    @Transaction
    @Insert
    fun insert(breedList: BreedsStateDbEntity)

    @Query("UPDATE breeds_state SET lastQueriedPage=:lastQueriedPage WHERE id = 0")
    fun updateLastQueriedPage(lastQueriedPage: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBreeds(breeds: List<BreedDbEntity>)

    @Query("SELECT lastQueriedPage FROM breeds_state WHERE id == 0")
    fun getLastQueriedPage(): Int

    @Query("SELECT * FROM breeds_state ORDER BY id LIMIT 1")
    fun getBreedState(): BreedsStateDbEntity?

    @Transaction
    @Query("SELECT * FROM breeds_state")
    fun getListWithBreeds(): List<ListWithBreeds>
}