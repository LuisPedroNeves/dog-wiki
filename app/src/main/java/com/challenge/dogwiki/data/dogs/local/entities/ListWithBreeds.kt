package com.challenge.dogwiki.data.dogs.local.entities

import androidx.room.Embedded
import androidx.room.Relation

data class ListWithBreeds(
    @Embedded val breedState: BreedsStateDbEntity,
    @Relation(
        parentColumn = "id",
        entityColumn = "breedListId"
    )
    val breeds: List<BreedDbEntity>
)