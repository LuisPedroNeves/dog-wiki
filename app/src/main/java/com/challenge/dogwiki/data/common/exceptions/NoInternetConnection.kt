package com.challenge.dogwiki.data.common.exceptions

import okio.IOException

class NoInternetConnection : IOException()