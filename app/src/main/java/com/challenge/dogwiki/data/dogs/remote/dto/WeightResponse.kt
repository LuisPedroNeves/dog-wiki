package com.challenge.dogwiki.data.dogs.remote.dto

import com.google.gson.annotations.SerializedName

data class WeightResponse(
    @SerializedName("imperial")
    val imperial: String,
    @SerializedName("metric")
    val metric: String
)