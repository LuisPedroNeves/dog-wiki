package com.challenge.dogwiki.presentation.breeds

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge.dogwiki.R
import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import com.challenge.dogwiki.domain.dogs.entity.BreedListEntity
import com.challenge.dogwiki.domain.dogs.usecases.GetDogBreedsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BreedsViewModel @Inject constructor(
    private val getDogBreedsUseCase: GetDogBreedsUseCase,
    private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _state = MutableStateFlow<BreedsFragmentState>(BreedsFragmentState.Init)
    val state: StateFlow<BreedsFragmentState> get() = _state

    private val _breeds = MutableStateFlow(BreedListEntity())
    val breeds: StateFlow<BreedListEntity> get() = _breeds

    private val _fabState = MutableStateFlow(FabMenuState())
    val fabState: StateFlow<FabMenuState> get() = _fabState

    private var loadingBreeds = false

    init {
        fetchDogBreeds()
    }

    private fun fetchDogBreeds(pageIndex: Int = 0) {
        viewModelScope.launch(ioDispatcher) {
            getDogBreedsUseCase.invoke(pageIndex)
                .onStart {
                    setLoading(pageIndex)
                    loadingBreeds = true
                }
                .catch { exception ->
                    hideLoading(pageIndex)
                    showToast(exception.message.toString())
                    loadingBreeds = false
                }
                .collect { result ->
                    hideLoading(pageIndex)
                    loadingBreeds = false

                    when (result) {
                        is BaseResult.Success -> {
                            handleResultData(result.data)
                        }
                        is BaseResult.Error -> {
                            if (result.data != null) {
                                handleResultData(result.data)
                            }
                            showToast(result.error)
                        }
                    }
                }
        }
    }

    private fun handleResultData(data: BreedListEntity) {
        _breeds.value = _breeds.value.copy(
            list = data.list,
            pageIndex = data.pageIndex
        )

        _fabState.value = FabMenuState(
            fabVisible = true,
            menuVisible = false,
            sortFabText = getSortFabText(),
            layoutFabText = getViewLayoutFabText()
        )
    }

    private fun setLoading(pageIndex: Int) {
        if (pageIndex == 0) {
            _state.value = BreedsFragmentState.LoadingVisibility(true)
        } else {
            _state.value = BreedsFragmentState.BottomLoadingVisibility(true)
        }
    }

    private fun hideLoading(pageIndex: Int) {
        if (pageIndex == 0) {
            _state.value = BreedsFragmentState.LoadingVisibility(false)
        } else {
            _state.value = BreedsFragmentState.BottomLoadingVisibility(false)
        }
    }

    private fun showToast(message: String) {
        _state.value = BreedsFragmentState.ShowToast(message)
    }

    fun onListScroll(canScrollVertically: Boolean) {
        if (!canScrollVertically && !loadingBreeds) {
            fetchDogBreeds(breeds.value.pageIndex + 1)
        }
    }

    fun onFabMenuClick() {
        val newMenuVisibility = !_fabState.value.menuVisible
        _fabState.value = _fabState.value.copy(
            menuVisible = newMenuVisibility,
            sortFabText = getSortFabText(),
            layoutFabText = getViewLayoutFabText()
        )
    }

    fun onSortFabClick() {
        val newIsListSorted = !_breeds.value.isListSorted
        val newList: List<BreedEntity> = if (newIsListSorted) {
            _breeds.value.list.sortedBy { it.name }
        } else {
            _breeds.value.list.toMutableList().apply { shuffle() }
        }

        _breeds.value = _breeds.value.copy(
            isListSorted = newIsListSorted,
            list = newList
        )

        _fabState.value = _fabState.value.copy(
            menuVisible = false
        )
    }

    fun onLayoutFabClick() {
        val newIsGridView = !_breeds.value.isGridView

        _breeds.value = _breeds.value.copy(
            isGridView = newIsGridView
        )

        _fabState.value = _fabState.value.copy(
            menuVisible = false
        )
    }

    private fun getSortFabText() = if (_breeds.value.isListSorted) {
        R.string.sort_randomly_label
    } else {
        R.string.sort_alphabetically_label
    }

    private fun getViewLayoutFabText() = if (_breeds.value.isGridView) {
        R.string.list_view_label
    } else {
        R.string.grid_view_label
    }

    sealed class BreedsFragmentState {
        object Init : BreedsFragmentState()
        data class LoadingVisibility(val isLoading: Boolean) : BreedsFragmentState()
        data class BottomLoadingVisibility(val isLoading: Boolean) : BreedsFragmentState()
        data class ShowToast(val message: String) : BreedsFragmentState()
    }

    data class FabMenuState(
        val fabVisible: Boolean = false,
        val menuVisible: Boolean = false,
        val sortFabText: Int = R.string.sort_randomly_label,
        val layoutFabText: Int = R.string.list_view_label
    )
}


