package com.challenge.dogwiki.presentation.breeds

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.challenge.dogwiki.R
import com.challenge.dogwiki.databinding.ItemGridBreedBinding
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity

class BreedsListAdapter(
    private val onItemClick: ((BreedEntity) -> Unit)
) : RecyclerView.Adapter<BreedsListAdapter.BreedViewHolder>() {

    private var breedsList = mutableListOf<BreedEntity>()

    fun setList(newList: List<BreedEntity>) {
        breedsList.clear()
        breedsList.addAll(newList)
        notifyDataSetChanged()
    }

    inner class BreedViewHolder(
        private val itemBinding: ItemGridBreedBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(breed: BreedEntity) {
            Glide.with(itemView.context)
                .load(breed.image)
                .centerCrop()
                .placeholder(R.drawable.image_placeholder_icon)
                .into(itemBinding.itemImage)

            itemBinding.itemName.text = breed.name

            itemBinding.root.setOnClickListener {
                onItemClick.invoke(breed)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BreedViewHolder {
        val view = ItemGridBreedBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BreedViewHolder(view)
    }

    override fun onBindViewHolder(holder: BreedViewHolder, position: Int) =
        holder.bind(breedsList[position])

    override fun getItemCount() = breedsList.size
}