package com.challenge.dogwiki.presentation.search

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.challenge.dogwiki.R
import com.challenge.dogwiki.databinding.FragmentSearchBreedBinding
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import com.challenge.dogwiki.domain.dogs.entity.SearchBreedEntity
import com.challenge.dogwiki.presentation.search.SearchBreedViewModel.SearchBreedFragmentState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


@AndroidEntryPoint
class SearchBreedFragment : Fragment(R.layout.fragment_search_breed) {

    private var _binding: FragmentSearchBreedBinding? = null
    private val binding get() = _binding!!

    private val viewModel: SearchBreedViewModel by viewModels()
    private lateinit var searchResultsAdapter: SearchBreedsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSearchBreedBinding.bind(view)

        setupRecyclerView()
        setupSearchView()
        setupObservers()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupRecyclerView() {
        searchResultsAdapter = SearchBreedsAdapter { item -> onItemClick(item) }

        binding.breedsRecyclerView.apply {
            adapter = searchResultsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun setupSearchView() {
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.onQueryTextSubmitted(query)
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                viewModel.onQueryTextChanged(query)
                return true
            }
        })
    }

    private fun setupObservers() {
        observeState()
        observeBreeds()
    }

    private fun observeState() {
        viewModel.state
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { state ->
                handleState(state)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun observeBreeds() {
        viewModel.breeds
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { breedsState ->
                handleBreeds(breedsState)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun handleState(state: SearchBreedFragmentState) {
        when (state) {
            is SearchBreedFragmentState.LoadingVisibility -> handleLoading(state.isLoading)
            is SearchBreedFragmentState.ShowToast -> showToast(state.message)
            is SearchBreedFragmentState.Init -> Unit
        }
    }

    private fun handleBreeds(breedsState: SearchBreedEntity) {
        searchResultsAdapter.setList(breedsState.list)
    }

    private fun handleLoading(isLoading: Boolean) {
        binding.loadingProgressBar.isVisible = isLoading
    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    private fun onItemClick(breedEntity: BreedEntity) {
        val bundle = bundleOf("breed_data" to breedEntity)
        findNavController().navigate(R.id.navigation_breed_details, bundle)
    }
}