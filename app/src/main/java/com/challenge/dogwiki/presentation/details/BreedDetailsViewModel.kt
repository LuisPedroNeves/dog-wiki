package com.challenge.dogwiki.presentation.details

import androidx.lifecycle.ViewModel
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

@HiltViewModel
class BreedDetailsViewModel @Inject constructor() : ViewModel() {

    private val _breedDetails = MutableStateFlow<BreedEntity?>(null)
    val breedDetails: StateFlow<BreedEntity?> get() = _breedDetails

    private lateinit var breedEntity: BreedEntity

    fun setup(breedEntity: BreedEntity) {
        this.breedEntity = breedEntity

        _breedDetails.value = this.breedEntity
    }
}