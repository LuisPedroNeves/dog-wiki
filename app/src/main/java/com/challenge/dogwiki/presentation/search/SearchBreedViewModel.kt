package com.challenge.dogwiki.presentation.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.entity.SearchBreedEntity
import com.challenge.dogwiki.domain.dogs.usecases.SearchDogBreedsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchBreedViewModel @Inject constructor(
    private val searchDogBreedsUseCase: SearchDogBreedsUseCase,
    private val ioDispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _state = MutableStateFlow<SearchBreedFragmentState>(SearchBreedFragmentState.Init)
    val state: StateFlow<SearchBreedFragmentState> get() = _state

    private val _breeds = MutableStateFlow(SearchBreedEntity())
    val breeds: StateFlow<SearchBreedEntity> get() = _breeds

    private var loadingBreeds = false

    private fun setLoading() {
        _state.value = SearchBreedFragmentState.LoadingVisibility(true)
    }

    private fun hideLoading() {
        _state.value = SearchBreedFragmentState.LoadingVisibility(false)
    }

    private fun showToast(message: String) {
        _state.value = SearchBreedFragmentState.ShowToast(message)
    }

    private fun searchDogBreeds(query: String) {
        viewModelScope.launch(ioDispatcher) {
            searchDogBreedsUseCase.invoke(query)
                .onStart {
                    setLoading()
                    loadingBreeds = true
                }
                .catch { exception ->
                    hideLoading()
                    showToast(exception.message.toString())
                    loadingBreeds = false
                }
                .collect { result ->
                    hideLoading()
                    loadingBreeds = false
                    when (result) {
                        is BaseResult.Success -> {
                            _breeds.value = _breeds.value.copy(
                                list = result.data.list,
                                query = result.data.query
                            )

                        }
                        is BaseResult.Error -> {
                            if (result.data != null) {
                                _breeds.value = _breeds.value.copy(
                                    list = result.data.list,
                                    query = result.data.query
                                )
                            }
                            showToast(result.error)
                        }
                    }
                }
        }
    }

    fun onQueryTextSubmitted(query: String?) {
        if (!query.isNullOrEmpty() && !loadingBreeds) {
            searchDogBreeds(query)
        }
    }

    fun onQueryTextChanged(query: String?) {
        if (query != _breeds.value.query && _breeds.value.list.isNotEmpty()) {
            _breeds.value = _breeds.value.copy(
                list = emptyList(),
                query = ""
            )
        }
    }

    sealed class SearchBreedFragmentState {
        object Init : SearchBreedFragmentState()
        data class LoadingVisibility(val isLoading: Boolean) : SearchBreedFragmentState()
        data class ShowToast(val message: String) : SearchBreedFragmentState()
    }
}


