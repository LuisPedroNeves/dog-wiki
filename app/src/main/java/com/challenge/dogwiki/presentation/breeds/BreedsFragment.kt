package com.challenge.dogwiki.presentation.breeds

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.challenge.dogwiki.R
import com.challenge.dogwiki.databinding.FragmentBreedsBinding
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import com.challenge.dogwiki.domain.dogs.entity.BreedListEntity
import com.challenge.dogwiki.presentation.breeds.BreedsViewModel.BreedsFragmentState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class BreedsFragment : Fragment(R.layout.fragment_breeds) {

    companion object {
        private const val NUMBER_OF_GRID_COLUMNS = 2
    }

    private var _binding: FragmentBreedsBinding? = null
    private val binding get() = _binding!!

    private val viewModel: BreedsViewModel by viewModels()
    private lateinit var breedsAdapter: BreedsListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentBreedsBinding.bind(view)

        setupRecyclerView()
        setupFabView()
        setupObservers()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupRecyclerView() {
        breedsAdapter = BreedsListAdapter { item -> onItemClick(item) }

        binding.breedsRecyclerView.apply {
            adapter = breedsAdapter
            layoutManager = GridLayoutManager(requireContext(), NUMBER_OF_GRID_COLUMNS)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0) {
                        viewModel.onListScroll(
                            recyclerView.canScrollVertically(1)
                        )
                    }
                }
            }
            )
        }
    }

    private fun setupFabView() {
        binding.fabMenu.setOnClickListener { viewModel.onFabMenuClick() }
        binding.sortFab.setOnClickListener { viewModel.onSortFabClick() }
        binding.layoutFab.setOnClickListener { viewModel.onLayoutFabClick() }
        binding.overlayView.setOnClickListener { viewModel.onFabMenuClick() }
    }

    private fun setupObservers() {
        observeState()
        observeBreeds()
        observeFabState()
    }

    private fun observeState() {
        viewModel.state
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { state ->
                handleState(state)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun observeBreeds() {
        viewModel.breeds
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { breedsState ->
                handleBreeds(breedsState)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun handleState(state: BreedsFragmentState) {
        when (state) {
            is BreedsFragmentState.LoadingVisibility -> handleLoading(state.isLoading)
            is BreedsFragmentState.BottomLoadingVisibility -> handleBottomLoading(state.isLoading)
            is BreedsFragmentState.ShowToast -> showToast(state.message)
            is BreedsFragmentState.Init -> Unit
        }
    }

    private fun handleBreeds(breedsState: BreedListEntity) {
        val currentManager = binding.breedsRecyclerView.layoutManager
        if (!breedsState.isGridView && currentManager is GridLayoutManager) {
            binding.breedsRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        } else if (breedsState.isGridView && currentManager !is GridLayoutManager) {
            binding.breedsRecyclerView.layoutManager =
                GridLayoutManager(requireContext(), NUMBER_OF_GRID_COLUMNS)
        }

        breedsAdapter.setList(breedsState.list)
    }

    private fun handleLoading(isLoading: Boolean) {
        binding.loadingProgressBar.isVisible = isLoading
    }

    private fun handleBottomLoading(isLoading: Boolean) {
        binding.bottomProgressBar.isVisible = isLoading
    }

    private fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    private fun observeFabState() {
        viewModel.fabState
            .flowWithLifecycle(viewLifecycleOwner.lifecycle, Lifecycle.State.STARTED)
            .onEach { state ->
                handleFabMenu(state)
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun handleFabMenu(state: BreedsViewModel.FabMenuState) {
        binding.fabMenu.isVisible = state.fabVisible
        binding.overlayView.isVisible = state.menuVisible

        binding.sortFab.isVisible = state.menuVisible
        binding.sortFabLabel.text = getString(state.sortFabText)
        binding.sortFabLabel.isVisible = state.menuVisible

        binding.layoutFab.isVisible = state.menuVisible
        binding.layoutFabLabel.text = getString(state.layoutFabText)
        binding.layoutFabLabel.isVisible = state.menuVisible
    }

    private fun onItemClick(breedEntity: BreedEntity) {
        val bundle = bundleOf("breed_data" to breedEntity)
        findNavController().navigate(R.id.navigation_breed_details, bundle)
    }
}
