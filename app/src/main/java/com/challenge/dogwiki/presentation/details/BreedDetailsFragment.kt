package com.challenge.dogwiki.presentation.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.challenge.dogwiki.R
import com.challenge.dogwiki.databinding.FragmentBreedDetailsBinding
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class BreedDetailsFragment : Fragment(R.layout.fragment_breed_details) {

    private var _binding: FragmentBreedDetailsBinding? = null
    private val binding get() = _binding!!

    private val viewModel: BreedDetailsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentBreedDetailsBinding.bind(view)

        setupViewModel()

        observe()
    }

    private fun observe() {
        observeBreedDetails()
    }

    private fun setupViewModel() {
        val breedEntity = arguments?.getParcelable<BreedEntity>("breed_data")
        if (breedEntity != null) {
            viewModel.setup(breedEntity)
        }
    }

    private fun observeBreedDetails() {
        viewModel.breedDetails.flowWithLifecycle(
            viewLifecycleOwner.lifecycle,
            Lifecycle.State.STARTED
        )
            .onEach { breed ->
                breed?.let { handleBreedDetails(breed) }
            }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun handleBreedDetails(breedEntity: BreedEntity) {
        Glide.with(requireContext())
            .load(breedEntity.image)
            .transform(RoundedCorners(40))
            .placeholder(R.drawable.image_placeholder_icon)
            .into(binding.topImage)

        binding.name.text = breedEntity.name
        binding.category.text = breedEntity.group
        binding.origin.text = breedEntity.origin
        binding.temperament.text = breedEntity.temperament
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}