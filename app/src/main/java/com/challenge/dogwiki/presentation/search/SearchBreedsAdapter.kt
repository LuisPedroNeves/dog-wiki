package com.challenge.dogwiki.presentation.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.challenge.dogwiki.databinding.ItemListBreedBinding
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity

class SearchBreedsAdapter(
    private val onItemClick: ((BreedEntity) -> Unit)
) : RecyclerView.Adapter<SearchBreedsAdapter.BreedViewHolder>() {

    private var breedsList = mutableListOf<BreedEntity>()

    fun setList(newList: List<BreedEntity>) {
        breedsList.clear()
        breedsList.addAll(newList)
        notifyDataSetChanged()
    }

    inner class BreedViewHolder(
        private val itemBinding: ItemListBreedBinding
    ) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(breed: BreedEntity) {
            itemBinding.itemName.text = breed.name
            itemBinding.itemGroup.text = breed.group
            itemBinding.itemOrigin.text = breed.origin

            itemBinding.root.setOnClickListener {
                onItemClick.invoke(breed)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BreedViewHolder {
        val view = ItemListBreedBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BreedViewHolder(view)
    }

    override fun onBindViewHolder(holder: BreedViewHolder, position: Int) =
        holder.bind(breedsList[position])

    override fun getItemCount() = breedsList.size
}