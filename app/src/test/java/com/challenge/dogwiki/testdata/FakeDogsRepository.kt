package com.challenge.dogwiki.testdata

import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.DogsRepository
import com.challenge.dogwiki.domain.dogs.entity.BreedListEntity
import com.challenge.dogwiki.domain.dogs.entity.SearchBreedEntity
import kotlinx.coroutines.flow.flow

class FakeDogsRepository(
    dogBreedsResponseInitial: BaseResult<BreedListEntity, String>? = null,
    searchDogBreedsResponseInitial: BaseResult<SearchBreedEntity, String>? = null,
) : DogsRepository {

    var dogBreedResponse = dogBreedsResponseInitial
    var searchDogBreedsResponse = searchDogBreedsResponseInitial

    override suspend fun getDogBreeds(pageIndex: Int) =
        flow {
            dogBreedResponse?.let { emit(dogBreedResponse!!) }
        }

    fun updateDogBreedResponse(updatedDogBreedsResponse: BaseResult<BreedListEntity, String>) {
        dogBreedResponse = updatedDogBreedsResponse
    }

    override suspend fun searchDogBreeds(query: String) =
        flow {
            searchDogBreedsResponse?.let { emit(searchDogBreedsResponse!!) }
        }

    fun updateSearchResponse(updatedSearchDogBreedsResponse: BaseResult<SearchBreedEntity, String>) {
        searchDogBreedsResponse = updatedSearchDogBreedsResponse
    }
}