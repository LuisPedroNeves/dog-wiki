package com.challenge.dogwiki

import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import com.challenge.dogwiki.presentation.details.BreedDetailsViewModel
import com.challenge.dogwiki.testdata.MainDispatcherRule
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class BreedDetailsViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Test
    fun `GIVEN on breed details WHEN setup is called THEN should notify UI of the breed details`() {
        // Setup
        val breedEntity = BreedEntity(0, "Affenpinscher", "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg", "Toy", "Germany", "Stubborn")
        val viewModel = BreedDetailsViewModel()

        // Run
        viewModel.setup(breedEntity)

        // Assert
        Assert.assertEquals(breedEntity, viewModel.breedDetails.value)
    }
}