package com.challenge.dogwiki

import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import com.challenge.dogwiki.domain.dogs.entity.BreedListEntity
import com.challenge.dogwiki.domain.dogs.usecases.GetDogBreedsUseCase
import com.challenge.dogwiki.presentation.breeds.BreedsViewModel
import com.challenge.dogwiki.testdata.FakeDogsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.Assert
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class BreedsViewModelTest {

    private val breedResultData = listOf(
        BreedEntity(
            0,
            "Affenpinscher",
            "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg",
            "Toy",
            "Germany",
            "Stubborn"
        ),
        BreedEntity(
            1,
            "Afghan Hound",
            "https://cdn2.thedogapi.com/images/hMyT4CDXR.jpg",
            "N/A",
            "Afghanistan",
            "Aloof"
        ),
        BreedEntity(
            2,
            "African Hunting Dog",
            "https://cdn2.thedogapi.com/images/rkiByec47.jpg",
            "N/A",
            "N/A",
            "Wild"
        )
    )

    private val breedLoadMoreResults = listOf(
        BreedEntity(
            0,
            "Affenpinscher",
            "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg",
            "Toy",
            "Germany",
            "Stubborn"
        ),
        BreedEntity(
            1,
            "Afghan Hound",
            "https://cdn2.thedogapi.com/images/hMyT4CDXR.jpg",
            "N/A",
            "Afghanistan",
            "Aloof"
        ),
        BreedEntity(
            2,
            "African Hunting Dog",
            "https://cdn2.thedogapi.com/images/rkiByec47.jpg",
            "N/A",
            "N/A",
            "Wild"
        ),
        BreedEntity(
            3,
            "Affenpinscher",
            "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg",
            "Toy",
            "Germany",
            "Stubborn"
        ),
        BreedEntity(
            4,
            "Afghan Hound",
            "https://cdn2.thedogapi.com/images/hMyT4CDXR.jpg",
            "N/A",
            "Afghanistan",
            "Aloof"
        ),
    )

    @Test
    fun `GIVEN on Breeds List screen WHEN initializing THEN should fetch breeds AND notify UI`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val expectedState = BreedsViewModel.BreedsFragmentState.LoadingVisibility(false)
            val breedsResultData = BreedListEntity(breedResultData)

            val fakeRepository =
                FakeDogsRepository(dogBreedsResponseInitial = BaseResult.Success(breedsResultData))
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)
            advanceUntilIdle()
            val breeds = viewModel.breeds.value
            val state = viewModel.state.value

            // Assert
            Assert.assertEquals(breedsResultData, breeds)
            Assert.assertEquals(expectedState, state)
        }

    @Test
    fun `GIVEN on Breeds List screen WHEN initializing AND no internet is available THEN should show an error`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val message = "No internet connection."
            val expectedState = BreedsViewModel.BreedsFragmentState.ShowToast(message)

            val fakeRepository =
                FakeDogsRepository(dogBreedsResponseInitial = BaseResult.Error("No internet connection."))
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)
            advanceUntilIdle()
            val state = viewModel.state.value

            // Assert
            Assert.assertEquals(expectedState, state)
        }

    @Test
    fun `GIVEN on Breeds List screen WHEN initializing AND no internet is available AND values saved on DB THEN should show an error AND list breeds`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val message = "No internet connection."
            val expectedState = BreedsViewModel.BreedsFragmentState.ShowToast(message)
            val breedsResultData = BreedListEntity(breedResultData)

            val fakeRepository = FakeDogsRepository(
                dogBreedsResponseInitial = BaseResult.Error(
                    "No internet connection.",
                    breedsResultData
                )
            )
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)

            advanceUntilIdle()
            val breeds = viewModel.breeds.value
            val state = viewModel.state.value

            // Assert
            Assert.assertEquals(breedsResultData, breeds)
            Assert.assertEquals(expectedState, state)
        }

    @Test
    fun `GIVEN on Breeds List screen WHEN breeds list gets scrolled until the end THEN load more breeds`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val breedsResultData = BreedListEntity(breedResultData)
            val loadMoreResultData = BreedListEntity(breedLoadMoreResults, 1)

            val fakeRepository =
                FakeDogsRepository(dogBreedsResponseInitial = BaseResult.Success(breedsResultData))
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)
            fakeRepository.updateDogBreedResponse(BaseResult.Success(loadMoreResultData))
            viewModel.onListScroll(false)

            advanceUntilIdle()
            val breeds = viewModel.breeds.value

            // Assert
            Assert.assertEquals(loadMoreResultData, breeds)
        }


    @Test
    fun `GIVEN on Breeds List screen WHEN breeds list gets scrolled until the end AND no internet connection while loading more THEN should show an error`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val message = "No internet connection."
            val expectedState = BreedsViewModel.BreedsFragmentState.ShowToast(message)
            val breedsResultData = BreedListEntity(breedResultData)

            val fakeRepository =
                FakeDogsRepository(dogBreedsResponseInitial = BaseResult.Success(breedsResultData))
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)
            fakeRepository.updateDogBreedResponse(BaseResult.Error(message))
            viewModel.onListScroll(false)
            advanceUntilIdle()
            val state = viewModel.state.value

            // Assert
            Assert.assertEquals(expectedState, state)
        }


    @Test
    fun `GIVEN on Breeds List screen WHEN fab menu clicked THEN should notify UI of fab state`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val expectedState = BreedsViewModel.FabMenuState(
                fabVisible = true,
                menuVisible = true,
                sortFabText = R.string.sort_randomly_label,
                layoutFabText = R.string.list_view_label
            )
            val breedsResultData = BreedListEntity(breedResultData)

            val fakeRepository =
                FakeDogsRepository(dogBreedsResponseInitial = BaseResult.Success(breedsResultData))
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)
            advanceUntilIdle()
            viewModel.onFabMenuClick()
            advanceUntilIdle()
            val fabState = viewModel.fabState.value

            // Assert
            Assert.assertEquals(expectedState, fabState)
        }


    @Test
    fun `GIVEN on Breeds List screen WHEN sort fab is clicked THEN should post a randomized list`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val expectedState = BreedsViewModel.FabMenuState(
                fabVisible = true,
                menuVisible = false,
                sortFabText = R.string.sort_randomly_label,
                layoutFabText = R.string.list_view_label
            )
            val breedsResultData = BreedListEntity(breedResultData)

            val fakeRepository =
                FakeDogsRepository(dogBreedsResponseInitial = BaseResult.Success(breedsResultData))
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)
            advanceUntilIdle()
            viewModel.onSortFabClick()
            advanceUntilIdle()
            val breeds = viewModel.breeds.value
            val fabState = viewModel.fabState.value

            // Assert
            Assert.assertEquals(false, breeds.isListSorted)
            Assert.assertNotEquals(breedsResultData.list, breeds.list)
            Assert.assertEquals(expectedState, fabState)
        }

    @Test
    fun `GIVEN on Breeds List screen WHEN sort fab is clicked AND list was randomized THEN should post ordered list`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val expectedState = BreedsViewModel.FabMenuState(
                fabVisible = true,
                menuVisible = false
            )
            val breedsResultData = BreedListEntity(breedResultData)

            val fakeRepository =
                FakeDogsRepository(dogBreedsResponseInitial = BaseResult.Success(breedsResultData))
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)
            advanceUntilIdle()
            viewModel.onSortFabClick()
            advanceUntilIdle()
            viewModel.onSortFabClick()
            advanceUntilIdle()
            val breeds = viewModel.breeds.value
            val fabState = viewModel.fabState.value

            // Assert
            Assert.assertEquals(true, breeds.isListSorted)
            Assert.assertEquals(breedsResultData.list, breeds.list)
            Assert.assertEquals(expectedState, fabState)
        }

    @Test
    fun `GIVEN on Breeds List screen WHEN layout fab is clicked THEN should change list layout`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val expectedState = BreedsViewModel.FabMenuState(
                fabVisible = true,
                menuVisible = false
            )
            val breedsResultData = BreedListEntity(breedResultData)

            val fakeRepository =
                FakeDogsRepository(dogBreedsResponseInitial = BaseResult.Success(breedsResultData))
            val useCase = GetDogBreedsUseCase(fakeRepository)

            // Run
            val viewModel = BreedsViewModel(useCase, dispatcher)
            advanceUntilIdle()
            viewModel.onLayoutFabClick()
            advanceUntilIdle()
            val breeds = viewModel.breeds.value
            val fabState = viewModel.fabState.value

            // Assert
            Assert.assertEquals(false, breeds.isGridView)
            Assert.assertEquals(expectedState, fabState)
        }
}