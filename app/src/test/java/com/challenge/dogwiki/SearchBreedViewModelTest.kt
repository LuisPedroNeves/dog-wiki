package com.challenge.dogwiki

import com.challenge.dogwiki.domain.common.BaseResult
import com.challenge.dogwiki.domain.dogs.entity.BreedEntity
import com.challenge.dogwiki.domain.dogs.entity.SearchBreedEntity
import com.challenge.dogwiki.domain.dogs.usecases.SearchDogBreedsUseCase
import com.challenge.dogwiki.presentation.search.SearchBreedViewModel
import com.challenge.dogwiki.testdata.FakeDogsRepository
import com.challenge.dogwiki.testdata.MainDispatcherRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

@OptIn(ExperimentalCoroutinesApi::class)
class SearchBreedViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Test
    fun `GIVEN on Search Breed screen WHEN text query is submitted THEN should search dog breeds AND notify UI`() =
        runTest {
            // Setup
            val dispatcher = StandardTestDispatcher(testScheduler)
            val searchResultData = SearchBreedEntity(
                listOf(
                    BreedEntity(
                        0,
                        "Affenpinscher",
                        "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg",
                        "Toy",
                        "Germany",
                        "Stubborn"
                    ),
                    BreedEntity(
                        1,
                        "Afghan Hound",
                        "https://cdn2.thedogapi.com/images/hMyT4CDXR.jpg",
                        "N/A",
                        "Afghanistan",
                        "Aloof"
                    ),
                    BreedEntity(
                        2,
                        "African Hunting Dog",
                        "https://cdn2.thedogapi.com/images/rkiByec47.jpg",
                        "N/A",
                        "N/A",
                        "Wild"
                    ),
                ),
                "test"
            )

            val fakeRepository = FakeDogsRepository(
                searchDogBreedsResponseInitial = BaseResult.Success(searchResultData)
            )
            val useCase = SearchDogBreedsUseCase(fakeRepository)
            val viewModel = SearchBreedViewModel(useCase, dispatcher)

            // Run
            viewModel.onQueryTextSubmitted("test")
            advanceUntilIdle()
            val breeds = viewModel.breeds.value

            // Assert
            assertEquals(searchResultData, breeds)
        }

    @Test
    fun `GIVEN on Search Breed screen WHEN text query is submitted AND an error occurs THEN should notify UI`() = runTest {
        // Setup
        val dispatcher = StandardTestDispatcher(testScheduler)
        val message = "No internet connection"
        val expectedState = SearchBreedViewModel.SearchBreedFragmentState.ShowToast(message)
        val fakeRepository =
            FakeDogsRepository(searchDogBreedsResponseInitial = BaseResult.Error(message))
        val useCase = SearchDogBreedsUseCase(fakeRepository)
        val viewModel = SearchBreedViewModel(useCase, dispatcher)

        // Run
        viewModel.onQueryTextSubmitted("test")
        advanceUntilIdle()
        val state = viewModel.state.value

        // Assert
        assertEquals(expectedState, state)
    }

    @Test
    fun `GIVEN on Search Breed screen WHEN text query is changed THEN should clear list AND notify UI`() = runTest {
        // Setup
        val dispatcher = StandardTestDispatcher(testScheduler)
        val expectedState = SearchBreedEntity()
        val searchResultData = SearchBreedEntity(
            listOf(
                BreedEntity(
                    0,
                    "Affenpinscher",
                    "https://cdn2.thedogapi.com/images/BJa4kxc4X.jpg",
                    "Toy",
                    "Germany",
                    "Stubborn"
                ),
                BreedEntity(
                    1,
                    "Afghan Hound",
                    "https://cdn2.thedogapi.com/images/hMyT4CDXR.jpg",
                    "N/A",
                    "Afghanistan",
                    "Aloof"
                ),
                BreedEntity(
                    2,
                    "African Hunting Dog",
                    "https://cdn2.thedogapi.com/images/rkiByec47.jpg",
                    "N/A",
                    "N/A",
                    "Wild"
                ),
            ),
            "test"
        )

        val fakeRepository =
            FakeDogsRepository(searchDogBreedsResponseInitial = BaseResult.Success(searchResultData))
        val useCase = SearchDogBreedsUseCase(fakeRepository)
        val viewModel = SearchBreedViewModel(useCase, dispatcher)

        // Run
        viewModel.onQueryTextSubmitted("test")
        advanceUntilIdle()
        viewModel.onQueryTextChanged("")
        advanceUntilIdle()
        val breeds = viewModel.breeds.value

        // Assert
        assertEquals(expectedState, breeds)
    }
}