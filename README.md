# Dog Wiki challenge
 * Implemented a dog api consumer app that lists dog breeds and allows search
 * Used clean architecture principles, separating data, domain and presentation layers
 * Used view models to control UI, use cases to establish bridge between presentation and data layer
 * Used Retrofit for network calls, Room for the database, Hilt/Dagger for dependency injection
 * Used flow/coroutines for multithreading and glide for image processing
